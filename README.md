# d02k3pi

## Setup
Add a path to where you want data to be saved to the setup.sh script. If you want to generate toys too, AmpGen will be needed and see further instructions in Toy Generation.

## Toy Generation
The code for generating toys lives in D02K3PiAnalysis/toyGen.
An external package called AmpGen is needed to generate the toys (https://github.com/GooFit/AmpGen). Compile AmpGen in an area of your choice and add the path to it in setup.sh.
To generate either Right Sign ($D^{0} to K^{+}\pi^{-}\pi^{+}\pi^{-}$) or Wrong Sign ($D^{0} to K^{-}\pi^{+}\pi^{+}\pi^{-}$) decays call the script
```bash
cd D02K3PiAnalysis/toyGen
. toy-generation.sh {WS/RS} {N EVENTS}
```
Alternatively to generate 10000 million events of each on the Condor batch system, call 
```bash
. toys-cluster-job.sh
```
And the toys will be saved in the data directory of the file.

To then generate toys with mixing including call
```bash
./main
```
from the toyGen directory which will save toys that mix the RS and WS decays according to come pre-defined pescription.