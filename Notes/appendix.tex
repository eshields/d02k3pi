% !TEX root = notes.tex
\appendix
\section{Appendices}
\label{sec:appendix}

\subsection{\Dz--\Dzb Mixing}
\label{sec:appendix:dzmixing}
The neutral charm mesons system can be described as a two quantum state system governed by the Schrodinger's equation
\begin{align}
  \frac{d}{dt}\ket{\psi} = \mathcal{H}\ket{\psi} = \left( {\bf M}+\frac{i}{2} {\bf \Gamma}\right)\ket{\psi},
\end{align}
in which
\begin{align*}
    {\bf M} = \left(
    \begin{matrix}
      M_{11} M_{12} \\
      M_{12}^* M_{11}
    \end{matrix}
      \right)\text{ and }{\bf \Gamma} = \left(
    \begin{matrix}
      \Gamma_{11} \Gamma_{12}\\
      \Gamma_{12}^* \Gamma_{11}
    \end{matrix}\right).
\end{align*}
It can be noted that \CPT invariance has been assumed, implying the same value for the elements on the diagonal.
If the non-diagonal elements of the matrices are different from zero, the physical eigenstates are not equal to the mass eigenstates and mixing occours.
The eigenvalues of the Schrodinger's equation are indeed
\begin{align}
\lambda_{1,2}=m_{1,2}+\frac{i}{2}\Gamma_{1,2},
\end{align}
where $m_{1,2}=M_{11}\pm |M_{12}|$ and $\Gamma_{1,2}=\Gamma_{11}\pm|\Gamma_{12}|$.

Considering that the $D_{1,2}$ can be expressed in terms of the flavor eigenstates
\begin{align}\label{eq:appendix:dcomb}
  \ket{D_1}   &= p\ket{\Dz} + q\ket{\Dzb}\nonumber\\
  \ket{D_2}   &= p\ket{\Dz} - q\ket{\Dzb},
\end{align}
with $p^2+q^2=1$, and that $(\mathcal{H}-\lambda_{1,2}\mathcal{I})=0$, one obtains a relationship between $q$ and $p$ and the off-diagonal elements of the matrix
\begin{align}
  \frac{p}{q} = \sqrt{\frac{M_{12}^*-\frac{i}{2}\Gamma^*_{12}}{M_{12}-\frac{i}{2}\Gamma_{12}}}=\left| \frac{p}{q}\right|e^{i\phi}.
\end{align}

The interesting part of the phenomenon is highlighted once time dependency is introduced
\begin{align}
  \ket{D_{1,2}(t)} = \hat{U}_{1,2}(t)\ket{D_{1,2}(0)},
\end{align}
where $\hat{U}_{1,2}(t)=e^{-i\lambda_{1,2}t}$ are time-evolution operators.
These operators can also be expressed as
\begin{align}
  \hat{U}_{1,2}(t)&=e^{-i\lambda_{1,2}t}\nonumber\\
    &= e^{im_{1,2}t}e^{\frac{-\Gamma_{1,2}}{2}t}\nonumber\\
    &= e^{imt}e^{-\frac{1}{2}\Gamma t}e^{\mp(y+ix)\frac{\Gamma}{2} t},
\end{align}
with the newly introduced symbols
\begin{align}
  m = \frac{m_1+m_2}{2}       \qquad& \Gamma = \frac{\Gamma_1+\Gamma_2}{2}\nonumber\\
  x = \frac{m_1-m_2}{\Gamma}  \qquad& y=\frac{\Gamma_1-\Gamma_2}{2\Gamma}.
\end{align}

By reverting Eq.~\ref{eq:appendix:dcomb},
\begin{align}
  \ket{\Dz}   &= \frac{1}{2p}\left(\ket{D_1} + \ket{D_2}\right)\nonumber\\
  \ket{\Dzb}  &= \frac{1}{2q}\left(\ket{D_1} - \ket{D_2}\right)
\end{align}
and introducing the time-evolution operators
\begin{align}
  \ket{\Dz(t)}   &= \frac{1}{2p}\left(\hat{U}_1(t)\ket{D_1(0)} + \hat{U}_2(t)\ket{D_2(0)}\right)\nonumber\\
  \ket{\Dzb(t)}  &= \frac{1}{2q}\left(\hat{U}_1(t)\ket{D_1(0)} - \hat{U}_2(t)\ket{D_2(0)}\right),
\end{align}
which can be expressed in terms of flavor eigenstates only when considering Eq.~\ref{eq:appendix:dcomb} for $t=0$
\begin{align}
  \ket{\Dz(t)}   &= \frac{1}{2p}\left(\hat{U}_1(t)\left(p\ket{\Dz} + q\ket{\Dzb}\right) + \hat{U}_2(t)\left(p\ket{\Dz} - q\ket{\Dzb}\right)\right)\nonumber\\
      &=\frac{\hat{U}_1(t)+\hat{U}_2(t)}{2}\ket{\Dz} + \frac{q}{p}\frac{\hat{U}_1(t)-\hat{U}_2(t)}{2}\ket{\Dzb}\nonumber\\
  \ket{\Dzb(t)}  &= \frac{1}{2q}\left(\hat{U}_1(t)\left(p\ket{\Dz} + q\ket{\Dzb}\right) - \hat{U}_2(t)\left(p\ket{\Dz} - q\ket{\Dzb}\right)\right)\nonumber\\
      &=\frac{\hat{U}_1(t)+\hat{U}_2(t)}{2}\ket{\Dzb} + \frac{p}{q}\frac{\hat{U}_1(t)-\hat{U}_2(t)}{2}\ket{\Dz}.
\end{align}
Therefore the fraction of the opposite flavor eigenstate increases with time.

When performing a measurement, the state is contracted to a certain final state $\bra{f}$.
The expectation value of the \decay{\Dz}{f} process is given by $A^2_f=|\braket{f|\mathcal{H}|\Dz}|^2$.
By considering the time evolution of the states, one has
\begin{align}
  A_f(t)&=\braket{f|\mathcal{H}|\Dz(t)} =\frac{\hat{U}_1(t)+\hat{U}_2(t)}{2}A_f + \frac{q}{p}\frac{\hat{U}_1(t)-\hat{U}_2(t)}{2}\overline{A}_f\nonumber\\
  \overline{A}_f(t)&=\braket{f|\mathcal{H}|\Dzb(t)}=\frac{\hat{U}_1(t)+\hat{U}_2(t)}{2}\overline{A}_f + \frac{p}{q}\frac{\hat{U}_1(t)-\hat{U}_2(t)}{2}A_f.
\end{align}
For measuring the squared amplitude it is more convenient to group on the time-evolution operators
\begin{align}
  A_f(t)&=\frac{\hat{U}_1(t)}{2}\left(A_f+ \frac{q}{p}\overline{A}_f\right) +\frac{\hat{U}_2(t)}{2}\left(A_f- \frac{q}{p}\overline{A}_f\right) \nonumber\\
  \overline{A}_f(t)&=\frac{\hat{U}_1(t)}{2}\left(\overline{A}_f+ \frac{p}{q}A_f\right) +\frac{\hat{U}_2(t)}{2}\left(\overline{A}_f- \frac{p}{q}A_f\right)
\end{align}
and remind that
\begin{align}
  |\hat{U}_1(t)|^2 &= e^{imt}e^{-\frac{1}{2}\Gamma t}e^{(-y-ix)\frac{\Gamma}{2} t} e^{-imt}e^{-\frac{1}{2}\Gamma t}e^{(-y+ix)\frac{\Gamma}{2} t}= e^{-(1+y)\Gamma t}\nonumber\\
  \hat{U}_1(t)\hat{U}_2^*(t) &= e^{imt}e^{-\frac{1}{2}\Gamma t}e^{(-y-ix)\frac{\Gamma}{2} t} e^{-imt}e^{-\frac{1}{2}\Gamma t}e^{(y-ix)\frac{\Gamma}{2} t} = e^{-(1+ix)\Gamma t} \nonumber\\
  \hat{U}_1^*(t)\hat{U}_2(t) &= e^{-imt}e^{-\frac{1}{2}\Gamma t}e^{(-y+ix)\frac{\Gamma}{2} t} e^{+imt}e^{-\frac{1}{2}\Gamma t}e^{(y+ix)\frac{\Gamma}{2} t} = e^{-(1-ix)\Gamma t}\nonumber\\
  |\hat{U}_2(t)|^2 &= e^{+imt}e^{-\frac{1}{2}\Gamma t}e^{(y+ix)\frac{\Gamma}{2} t}e^{-imt}e^{-\frac{1}{2}\Gamma t}e^{(y-ix)\frac{\Gamma}{2} t} = e^{-(1-y)\Gamma t}.
\end{align}
One therefore obtains
\begin{align}
  |A_f(t)|^2 = \frac{1}{4} &\left[|\hat{U}_1(t)|^2 \left|A_f+ \frac{q}{p}\overline{A}_f\right|^2 \right. \nonumber\\
          &+\hat{U}_1(t)\hat{U}_2^*(t) \left(A_f+ \frac{q}{p}\overline{A}_f\right)\left(A_f- \frac{q}{p}\overline{A}_f\right)^* \nonumber\\
          &+\hat{U}_1(t)^*\hat{U}_2(t) \left(A_f+ \frac{q}{p}\overline{A}_f\right)^*\left(A_f- \frac{q}{p}\overline{A}_f\right) \nonumber\\
          &+\left.|\hat{U}_2(t)|^2\left|A_f- \frac{q}{p}\overline{A}_f\right|^2 \right]\\
          = \frac{1}{4}e^{-\Gamma t}& \left\{ e^{-y\Gamma t} \left[ |A_f|^2 +  \left|\frac{q}{p}\overline{A}_f\right|^2 + 2\mathcal{R}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \right] \right.\nonumber\\
          &+e^{-ix\Gamma t}\left[ |A_f|^2 - \left|\frac{q}{p}\overline{A}_f\right|^2 - 2i\mathcal{I}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \right]\nonumber\\
          &+e^{+ix\Gamma t}\left[ |A_f|^2 - \left|\frac{q}{p}\overline{A}_f\right|^2 + 2i\mathcal{I}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \right]\nonumber\\
          &\left.+e^{+y\Gamma t} \left[ |A_f|^2 +  \left|\frac{q}{p}\overline{A}_f\right|^2 - 2\mathcal{R}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \right] \right\}\\
          = \frac{1}{4}e^{-\Gamma t}& \left\{ |A_f|^2 (e^{-y\Gamma t}+e^{-ix\Gamma t}+e^{+ix\Gamma t}+e^{+y\Gamma t} )\right.\nonumber\\
          &+\left|\frac{q}{p}\overline{A}_f\right|^2 (e^{-y\Gamma t}-e^{-ix\Gamma t}-e^{+ix\Gamma t}+e^{+y\Gamma t} )\nonumber\\
          &+2\mathcal{R}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) (e^{-y\Gamma t}-e^{+y\Gamma t} )\nonumber\\
          &\left.+2i\mathcal{I}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right)(-e^{-ix\Gamma t}+e^{+ix\Gamma t} )\right\}\\
          = \frac{1}{2}e^{-\Gamma t}& \left\{ |A_f|^2 (\cos(x\Gamma t)+ \cosh(y\Gamma t) )\right.\qquad{\color{red}{2\cosh{x}=e^x+e^{-x},\phantom{p}2\cos{x}=e^{ix}+e^{-ix}}}\nonumber\\
          &+\left|\frac{q}{p}\overline{A}_f\right|^2 (-\cos(x\Gamma t)+ \cosh(y\Gamma t) )\nonumber\\
          &-2\mathcal{R}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \sinh(y\Gamma t )\qquad{\color{red}{2\sinh{x}=e^x-e^{-x}}}\nonumber\\
          &\left.-2\mathcal{I}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right)\sin(x\Gamma t )\right\}\qquad{\color{red}{2i\sin{x}=e^{ix}-e^{-ix}}}\\
          = \frac{1}{2}e^{-\Gamma t}& \left\{ \left( |A_f|^2 + \left|\frac{q}{p}\overline{A}_f\right|^2 \right) \cosh(y\Gamma t) +\left( |A_f|^2 - \left|\frac{q}{p}\overline{A}_f\right|^2 \right) \cos(x\Gamma t)\right.\nonumber\\
          &\left.-2\mathcal{R}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right) \sinh(y\Gamma t) -2\mathcal{I}\left(A_f\left[\frac{q}{p}\overline{A}_f\right]^*\right)\sin(x\Gamma t) \right\}
\end{align}
and similarly for the charged-conjugate
\begin{align}
  |\overline{A}_f(t)|^2  = \frac{1}{2}e^{-\Gamma t}& \left\{ \left( |\overline{A}_f|^2 + \left|\frac{p}{q}A_f\right|^2 \right) \cosh(y\Gamma t) +\left( |\overline{A}_f|^2 - \left|\frac{p}{q}A_f\right|^2 \right) \cos(x\Gamma t)\right.\nonumber\\
&\left.-2\mathcal{R}\left(\overline{A}_f\left[\frac{p}{q}A_f\right]^*\right) \sinh(y\Gamma t) -2\mathcal{I}\left(\overline{A}_f\left[\frac{p}{q}A_f\right]^*\right)\sin(x\Gamma t) \right\}
\end{align}
evidencing the mixture of \Dz and \Dzb in the final state.
