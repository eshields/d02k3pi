% !TEX root = notes.tex
\section{Formalism}
\label{sec:formalism}

\subsection{Mixing of \Dz mesons}
\label{sec:formalism:mixing}

The observed \Dz and \Dzb mesons can be expressed terms of two mass states
\begin{align*}
  \ket{\Dz}  &= p\ket{D_1} + q\ket{D_2}\\
  \ket{\Dzb} &= p\ket{D_1} - q\ket{D_2}
\end{align*}
Following quantum mechanics, this implies that the oscillation between the \Dz and \Dzb states is regulated by the differences of the mass eigenvalues $\Delta M = M_2-M_1$ and of the states widths $\Delta\Gamma=\Gamma_2-\Gamma_1$.
The experimental parameters are
\begin{align*}
  x = \frac{\Delta M}{\Gamma}\text{ and } y = \frac{\Delta\Gamma}{2\Gamma},
\end{align*}
where $\Gamma$ is the average between $\Gamma_1$ and $\Gamma_2$.
The current averages of the experimental results are $x=0.46\pm0.13\%$ and $y=0.62\pm0.07\%$, and mixing is clearly established.

\subsection{The Wrong-Sign to Right-Sign Ratio}
\label{sec:formalism:wsratio}
A way to measure mixing is to measure the ratio of so-called ``wrong-sign'' (WS) \Dz meson decays with respect to the ``right-sign'' (RS) over time.
In the case of four-body the WS are the \decay{\Dz}{\Kp\pim\pip\pim} decays and the RS are the \decay{\Dz}{\Km\pip\pip\pim} decays.
When ignoring mixing, the ratio between the two decay amplitudes is roughly $A^2_{DCS}/A_{CF}^2\approx0.1\%$.

The interesting thing is that the amplitude of the mixing diagram in charm decays is also at the order of the permille, therefore \Dz mesons have two paths to reach the final state $\Kp\pim\pim\pip$, one direct with a doubly Cabibbo suppressed amplitude, and another through mixing and the subsequent Cabibbo favored decay of the \Dzb meson.
These two paths have roughly the same amplitude and interference effects are significant, making ``wrong-sign'' modes sensitive probes of mixing parameters in Charm.

The decay amplitudes of the WS and RS decays can be written as
\begin{align*}
  \Gamma(\decay{\Dz}{\Kp\pim\pip\pim}) &\simeq \frac{1}{2}e^{-\Gamma t}\left(A_{DCS}^2 - A_{DCS}A_{CF}R_D^f(y\cos\delta_D^f-x\sin\delta_D^f)\Gamma t + A_{CF}^2\frac{x^2+y^2}{4}(\Gamma t)^2\right)\\
  \Gamma(\decay{\Dz}{\Km\pip\pip\pim}) &\simeq \frac{1}{2}e^{-\Gamma t}A_{CF}^2
\end{align*}
where $R_D^f$ is a ``coherence factor'' between 0 and 1 that dilutes the interference, $A_{CF,DCS}$ are the amplitudes of the Cabibbo favored and doubly Cabibbo suppressed decays and $\delta_D^f$ is the strong-phase difference between the $\Kp\pim\pip\pim$ and $\Km\pip\pip\pim$ final states.

The time dependance of the ratio between doubly Cabibbo suppressed and Cabibbo favored decays can be expressed as
\begin{align}
  R(t) &= \frac{\Gamma(\decay{\Dz}{\Kp\pim\pip\pim})}{\Gamma(\decay{\Dz}{\Km\pip\pip\pim})}\nonumber\\
       &\simeq \frac{A_{DCS}^2}{A_{CF}^2} - \frac{A_{DCS}}{A_{CF}}(y\cos\delta_D^f-x\sin\delta_D^f)\Gamma t + \frac{x^2+y^2}{4}(\Gamma t)^2\nonumber\\
       &= r_D^2 - r_D (y\cos\delta_D^f-x\sin\delta_D^f)\Gamma t + \frac{x^2+y^2}{4}(\Gamma t)^2
\end{align}
where $r_D=A_{DCS}/A_{CF}$ is used in the final equation.
It appears that the ratio can be expressed in terms of the phase-space-dependent parameters $r_D$, $\delta_D^f$ and the mixing parameters $x$ and $y$.

This ratio has been studied by integrating over all the events in the phase space of the decay by the \lhcb collaboration~\cite{}.
In that analysis it was only possible to check the validity of the mixing hypothesis, but no access was possible to the mixing parameters due to the presence of the coherence factor and the strong phases difference.

\subsection{Bin-flip}
\label{sec:formalism:binflip}
The idea to overcome the challenge of the coherence factor and gain sensitivity to the mixing parameters is to perform the measurement in separate regions of the phase space where the strong phase difference can be considered small.
The values of coherence factors in each phase space region can be extracted by experiments that record \Dz mesons from quantum-correlated \Dz--\Dzb production, such as BESIII and CLEO-c and used as constraints in the fit.
A similar approach has been successfully carried on by the \lhcb experiment~\cite{}.

The ratio is measured in regions of the phase space
\begin{align}
  R_b(t) &= \int_{b}R(t)d\phi\nonumber\\
   &= \int_{b}|r_D|^2d\phi + |z|\Gamma t\int_b |r_D|(\cos\delta_D y - \sin\delta_D x)d\phi + \frac{|z|^2}{4}(\Gamma t)^2\nonumber\\
   &= \int_{b}|r_D|^2d\phi + |z|\Gamma t \left(y \int_b |r_D|\cos\delta_D d\phi- x\int_b |r_D|\sin\delta_D d\phi\right) + \frac{|z|^2}{4}(\Gamma t)^2,
\end{align}
where $z=x+iy$ is used.
The term incapsulating the mixing parameters shows a dependency on the strong phase difference between the two decay amplitudes that, while making difficult to separate mixing and strong phase difference effects when integrating on the whole samples, allows to access the mixing parameters with varying sensitivity in the various regions of the phase space.
For example, some regions of the phase space are highly sensitive to $x$ ($\delta_D=\pm\pi/2$) or to $y$ ($\delta_D=0,\pi$).
