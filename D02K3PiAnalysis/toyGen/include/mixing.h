#ifndef _D02K3Pi_H_
#define _D02K3Pi_H_

// SL
#include <iostream>
#include <complex>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TChain.h"
#include "TLorentzVector.h"
#include "TRandom2.h"
#include "TStopwatch.h"
#include "TLeaf.h"


class Mixing
{
private:
    // Mixing parameters.
    double _x;
    double _y;

    // Output file.
    std::string _output;

    // Define input variables
    Double_t K[2][4];
    Double_t pi[2][4];
    Double_t pip[2][4];
    Double_t pim[2][4];

    // Define variables.
    Double_t mD, mK, mpi, mpip, mpim;
    Double_t mKpi, mKpip, mKpim, mpipip, mpipim, mpippim;
    Double_t mKpipip, mKpipim, mKpippim, mpipippim;
    Double_t D_pid, K_pid, pi_pid;
    Double_t t;

    TLorentzVector* _lv;

    // CKM matrix factors;
    std::complex< Double_t > Vcs, Vud, Vcd, Vus;


public:
    Mixing(const double& x, const double& y)
    {
        _x = x;
        _y = y;

        _lv = new TLorentzVector();

        Vcs = 1.;
        Vud = 1.;
        Vcd = 1.;
        Vus = 1.;

    }
    ~Mixing() {};

    TTree* mix(TTree* RS, TTree* WS, int toy_number, int n_events);

    void addEntry(TTree*& tr, int index);

    int decide(double_t& t, TTree*& WS, TRandom*& p);

    // Strong phase helpers
    Double_t R();
    Double_t delta();

    void set_output( std::string output ) { _output = output; }

    Double_t getMass(Double_t px, Double_t py, Double_t pz, Double_t E);

    TTree* defineTree();

    void configureRStree(TTree* tr);
    void configureWStree(TTree* tr);

    void saveOutput(TTree* tr, std::string output);

    // Getters.
    double x() { return _x; }
    double y() { return _y; }

    std::complex< Double_t > Acf()  { return Vcs*Vud; }
    std::complex< Double_t > Adcs() { return Vcd*Vus; }
    Double_t ASqcf()  { return std::pow( std::norm( Acf()  ) , 2 ); }
    Double_t ASqdcs() { return std::pow( std::norm( Adcs() ) , 2 ); }
};
#endif