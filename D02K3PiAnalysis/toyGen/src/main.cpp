#include "mixing.h"

// SL
#include <string>

// ROOT
#include "TChain.h"

TChain* getRSch()
{
    TChain* ch = new TChain("DalitzEventList");

    for (int i=0; i<10; i++)
    {
        std::string name = "/afs/cern.ch/user/e/eshields/cernbox/Analysis/d02k3pi/data/toys/D02K3PiRS"+std::to_string(i)+".root";
        ch->Add(name.c_str());
    }

    return ch;
}

TChain* getWSch()
{
    TChain* ch = new TChain("DalitzEventList");

    for (int i=0; i<10; i++)
    {
        std::string name = "/afs/cern.ch/user/e/eshields/cernbox/Analysis/d02k3pi/data/toys/D02K3PiWS"+std::to_string(i)+".root";
        ch->Add(name.c_str());
    }

    return ch;
}

int main(int argc, char** argv)
{   
    int n_events = 1000;
    int toy_number;
    if ( argv[1] ) { toy_number = std::stoi(argv[1]); }
    else { toy_number = 1; }

    Mixing m = Mixing(0.4,0.6);
    
    TChain* RStr = getRSch();
    TChain* WStr = getWSch();
    
    std::cout << "RS entries: " << RStr->GetEntries() << std::endl;

    std::string output = "/afs/cern.ch/user/e/eshields/cernbox/Analysis/d02k3pi/data/toys/d02k3pi_mixed_toy"+std::to_string(toy_number)+".root";
    m.set_output( output );
    TTree* tr = m.mix(RStr, WStr, toy_number, n_events);
    tr->Print();
    
    return 0;
}