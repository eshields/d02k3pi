#include "mixing.h"

TTree* Mixing::mix(TTree* RS, TTree* WS, int toy_number, int n_events)
{
    TTree* tr = defineTree();

    configureRStree(RS);
    configureWStree(WS);

    TRandom* rand = new TRandom2();
    rand->SetSeed(toy_number);
    TStopwatch* watch = new TStopwatch();
    watch->Start();
    // RS and WS event indices offset.
    int ri = ( toy_number * n_events );
    int wi = ( toy_number * n_events );
    for (int i=0; i<n_events; i++)
    {
        t = rand->Exp(1.);
        int index = decide(t, WS, rand);
        if ( index == 0 ) { ri++; }
        else { wi++; }
        tr->GetEntry(i);
        RS->GetEntry(ri);
        WS->GetEntry(wi);

        addEntry(tr, index);
    }
    watch->Stop();
    
    std::cout << "To produce " << tr->GetEntries() << "Events" << std::endl;
    std::cout << "CPU time: " << watch->CpuTime() << " s" << std::endl;
    std::cout << "Real time: " << watch->RealTime() << " s" << std::endl;

    saveOutput(tr, _output);
    
    return tr;
}


void Mixing::addEntry(TTree*& tr, int index)
{   
    // Particles masses.
    mD = getMass(K[index][0] + pi[index][0] + pip[index][0] + pim[index][0],
                 K[index][1] + pi[index][1] + pip[index][1] + pim[index][1],
                 K[index][2] + pi[index][2] + pip[index][2] + pim[index][2],
                 K[index][3] + pi[index][3] + pip[index][3] + pim[index][3]
                 );

    mK =   getMass( K[index][0],   K[index][1],   K[index][2],   K[index][3]   );
    mpi =  getMass( pi[index][0],  pi[index][1],  pi[index][2],  pi[index][3]  );
    mpip = getMass( pip[index][0], pip[index][1], pip[index][2], pip[index][3] );
    mpim = getMass( pim[index][0], pim[index][1], pim[index][2], pip[index][3] );

    // 2-body dalitz variables.
    mKpi =    getMass(K[index][0] + pi[index][0],
                      K[index][1] + pi[index][1],
                      K[index][2] + pi[index][2],
                      K[index][3] + pi[index][3] 
                     );
    mKpip =   getMass(K[index][0] + pip[index][0],
                      K[index][1] + pip[index][1],
                      K[index][2] + pip[index][2],
                      K[index][3] + pip[index][3] 
                     );
    mKpim =   getMass(K[index][0] + pim[index][0],
                      K[index][1] + pim[index][1],
                      K[index][2] + pim[index][2],
                      K[index][3] + pim[index][3] 
                     );
    mpipip =  getMass(pi[index][0] + pip[index][0],
                      pi[index][1] + pip[index][1],
                      pi[index][2] + pip[index][2],
                      pi[index][3] + pip[index][3] 
                     );
    mpipim =  getMass(pi[index][0] + pim[index][0],
                      pi[index][1] + pim[index][1],
                      pi[index][2] + pim[index][2],
                      pi[index][3] + pim[index][3] 
                     );
    mpippim = getMass(pim[index][0] + pip[index][0],
                      pim[index][1] + pip[index][1],
                      pim[index][2] + pip[index][2],
                      pim[index][3] + pip[index][3] 
                     );

    // 3-body dalitz variables.
    mKpipip =   getMass(K[index][0] + pi[index][0] + pip[index][0],
                        K[index][1] + pi[index][1] + pip[index][1],
                        K[index][2] + pi[index][2] + pip[index][2],
                        K[index][3] + pi[index][3] + pip[index][3]
                       );
    mKpipim =   getMass(K[index][0] + pi[index][0] + pim[index][0],
                        K[index][1] + pi[index][1] + pim[index][1],
                        K[index][2] + pi[index][2] + pim[index][2],
                        K[index][3] + pi[index][3] + pim[index][3]
                       );
    mKpippim =  getMass(K[index][0] + pip[index][0] + pim[index][0],
                        K[index][1] + pip[index][1] + pim[index][1],
                        K[index][2] + pip[index][2] + pim[index][2],
                        K[index][3] + pip[index][3] + pim[index][3]
                       );
    mpipippim = getMass(pi[index][0] + pip[index][0] + pim[index][0],
                        pi[index][1] + pip[index][1] + pim[index][1],
                        pi[index][2] + pip[index][2] + pim[index][2],
                        pi[index][3] + pip[index][3] + pim[index][3]
                       );
    D_pid = index;
    K_pid = index;
    pi_pid = index;

    tr->Fill();
}

int Mixing::decide(Double_t& t, TTree*& WS, TRandom*& rand)
{   
    Double_t WSgamma = std::norm( ASqdcs() - Adcs()*Acf()*R()*( y()*std::cos( delta() ) - x()*std::sin( delta() ) )*t + Acf()*( ( std::pow( x() , 2 ) + std::pow( y() , 2 ) ) / 4. )*(std::pow( t , 2 )) );
    Double_t RSgamma = std::norm( ASqcf() );
    Double_t prob = rand->Uniform(0,RSgamma+WSgamma);
    if ( prob > RSgamma ) return 1;
    return 0;
}

Double_t Mixing::R()
{
    return 1.;
}

Double_t Mixing::delta()
{
    return 1.;
}

Double_t Mixing::getMass(Double_t px, Double_t py, Double_t pz, Double_t E)
{
    _lv->SetPxPyPzE(px, py, pz, E);
    Double_t M = _lv->M();
    return M;
}

TTree* Mixing::defineTree()
{
    TTree* tree = new TTree("d02k3pi","d02k3pi");
    
    // Define mass variables.
    tree->Branch("mD",&mD,"mD/D");
    tree->Branch("mK",&mK,"mK/D");
    tree->Branch("mpi",&mpi,"mpi/D");
    tree->Branch("mpip",&mpip,"mpip/D");
    tree->Branch("mpim",&mpim,"mpim/D");

    // Define dalitz variables.
    tree->Branch("mKpi",&mKpi,"mKpi/D");
    tree->Branch("mKpip",&mKpip,"mKpip/D");
    tree->Branch("mKpim",&mKpim,"mKpim/D");
    tree->Branch("mpipip",&mpipip,"mpipip/D");
    tree->Branch("mpipim",&mpipim,"mpipim/D");
    tree->Branch("mpippim",&mpippim,"mpippim/D");

    // Define 3 body variables.
    tree->Branch("mKpipip",&mKpipip,"mKpipip/D");
    tree->Branch("mKpipim",&mKpipim,"mKpipim/D");
    tree->Branch("mKpippim",&mKpippim,"mKpippim/D");
    tree->Branch("mpipippim",&mpipippim,"mpipippim/D");

    // Other variables.
    tree->Branch("D_pid",&D_pid,"D_pid/D");
    tree->Branch("K_pid",&K_pid,"K_pid/D");
    tree->Branch("pi_pid",&pi_pid,"pi_pid/D");
    tree->Branch("t",&t,"t/D");
    
    return tree;
}

void Mixing::configureRStree(TTree* tr)
{
    tr->SetBranchAddress("_1_K#_Px",&K[0][0]);
    tr->SetBranchAddress("_1_K#_Py",&K[0][1]);
    tr->SetBranchAddress("_1_K#_Pz",&K[0][2]);
    tr->SetBranchAddress("_1_K#_E",&K[0][3]);

    tr->SetBranchAddress("_2_pi~_Px",&pi[0][0]);
    tr->SetBranchAddress("_2_pi~_Py",&pi[0][1]);
    tr->SetBranchAddress("_2_pi~_Pz",&pi[0][2]);
    tr->SetBranchAddress("_2_pi~_E",&pi[0][3]);

    tr->SetBranchAddress("_3_pi~_Px",&pip[0][0]);
    tr->SetBranchAddress("_3_pi~_Py",&pip[0][1]);
    tr->SetBranchAddress("_3_pi~_Pz",&pip[0][2]);
    tr->SetBranchAddress("_3_pi~_E",&pip[0][3]);

    tr->SetBranchAddress("_4_pi#_Px",&pim[0][0]);
    tr->SetBranchAddress("_4_pi#_Py",&pim[0][1]);
    tr->SetBranchAddress("_4_pi#_Pz",&pim[0][2]);
    tr->SetBranchAddress("_4_pi#_E",&pim[0][3]);
}

void Mixing::configureWStree(TTree* tr)
{
    tr->SetBranchAddress("_2_K~_Px",&K[1][0]);
    tr->SetBranchAddress("_2_K~_Py",&K[1][1]);
    tr->SetBranchAddress("_2_K~_Pz",&K[1][2]);
    tr->SetBranchAddress("_2_K~_E",&K[1][3]);

    tr->SetBranchAddress("_1_pi#_Px",&pi[1][0]);
    tr->SetBranchAddress("_1_pi#_Py",&pi[1][1]);
    tr->SetBranchAddress("_1_pi#_Pz",&pi[1][2]);
    tr->SetBranchAddress("_1_pi#_E",&pi[1][3]);

    tr->SetBranchAddress("_3_pi~_Px",&pip[1][0]);
    tr->SetBranchAddress("_3_pi~_Py",&pip[1][1]);
    tr->SetBranchAddress("_3_pi~_Pz",&pip[1][2]);
    tr->SetBranchAddress("_3_pi~_E",&pip[1][3]);

    tr->SetBranchAddress("_4_pi#_Px",&pim[1][0]);
    tr->SetBranchAddress("_4_pi#_Py",&pim[1][1]);
    tr->SetBranchAddress("_4_pi#_Pz",&pim[1][2]);
    tr->SetBranchAddress("_4_pi#_E",&pim[1][3]);
}

void Mixing::saveOutput(TTree* tr, std::string output)
{
    TFile* Mfile = new TFile(output.c_str(),"RECREATE");
    Mfile->cd();
    tr->Write();
    Mfile->Close();

    std::cout << "Output written to: " << output << std::endl;
}