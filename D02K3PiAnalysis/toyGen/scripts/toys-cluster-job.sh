#!/bin/bash

# Define variables.
TOYROOT=$(pwd)
NJOBS=9
NPERJOB=1000000

# Define ganag script.
SUBMIT=${TOYROOT}/gangajobs/d02k3pi_job.ganga.py
if [ -f "${SUBMIT}" ]; then rm ${SUBMIT}; fi
touch ${SUBMIT}

# Write executables and add them to ganga script.
echo "exePaths = [" >> ${SUBMIT}
for SIGN in RS WS
do
    for SUBJOB in $(seq 0 ${NJOBS})
    do
        FILE=${TOYROOT}/gangajobs/d02k3pi_subjob${SIGN}${SUBJOB}.sh
        if [ -f "$FILE" ]
        then
            rm ${FILE}
        fi
        touch ${FILE}
        echo "#!/bin/bash" >> ${FILE}
        # Setup environment
        echo "export D02K3PiROOT=${D02K3PiROOT}" >> ${FILE}
        echo "export AMPGENROOT=${AMPGENROOT}" >> ${FILE}
        echo "source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc8-opt" >> ${FILE}
        echo "source ${TOYROOT}/toy-generation.sh ${SIGN} ${NPERJOB} ${SUBJOB}" >> ${FILE}

        echo "\"${FILE}\"," >> ${SUBMIT}
    done 
done 

echo "]" >> ${SUBMIT}
echo "" >> ${SUBMIT}

# Add ganga instructions to ganga script.
cat ${SUBMIT}.tmp >> ${SUBMIT}

# Submit to ganga.
cd ${HOME}
ganga ${SUBMIT}
cd ${TOYROOT}
