#!/bin/bash

#Maybe add add options?
DECAY=$1
NEVN=$2
SUBJOB=$3

if [[ "${DECAY}" == "D02Kpipipi" || "${DECAY}" == "RS" ]]
then
    echo "Producing ${NEVN} D0 -> K- pi+ pi+ pi+ (RS) decays"
    ${AMPGENROOT}/build/bin/Generator ${AMPGENROOT}/options/D02Kpipipi.opt --EventType "D0 K- pi+ pi+ pi-" --nEvents ${NEVN} --Output=${D02K3PiROOT}/data/toys/D02K3PiRS${SUBJOB}.root
elif [[ "${DECAY}" == "D02piKpipi" || "${DECAY}" == "WS" ]]
then 
    echo "Producing ${NEVN} D0 -> K+ pi- pi+ pi+ (WS) decays"
    ${AMPGENROOT}/build/bin/Generator ${AMPGENROOT}/options/D02piKpipi.opt --EventType "D0 pi- K+ pi+ pi-" --nEvents ${NEVN} --Output=${D02K3PiROOT}/data/toys/D02K3PiWS${SUBJOB}.root
fi