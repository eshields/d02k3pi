#!/bin/bash

export D02K3PiROOT=${PWD}

#Add library and AmpGen to environment

rm data
if [ "${USER}" == "eshields" ]
then
  ln -s /eos/lhcb/user/e/eshields/D02K3Pi/data data
  export AMPGENROOT=${HOME}/cernbox/Analysis/AmpGen/
fi
